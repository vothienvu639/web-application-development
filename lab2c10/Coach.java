package lab2c10;

public interface Coach {
    String getDailyWorkout();
    String getDailyFortune();
}
