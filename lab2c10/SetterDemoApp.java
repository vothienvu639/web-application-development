package lab2c10;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SetterDemoApp {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("lab2c10/applicationContext.xml");
        CricketCoach theCoach = context.getBean("myCricketCoach", CricketCoach.class);
        FortuneService fortuneService = new HappyFortuneService();
        theCoach.setFortuneService(fortuneService);
        theCoach.setTeam("Sunriser Hyderabad");
        theCoach.setEmailAddress("thebestcoach@Luv2code.com");
        System.out.println(theCoach.getDailyFortune());
        System.out.println(theCoach.getDailyWorkout());
        System.out.println(theCoach.getEmailAddress());
        System.out.println(theCoach.getTeam());
        context.close();
    }
}
