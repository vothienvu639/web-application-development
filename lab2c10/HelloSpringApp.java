package lab2c10;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("EcerciseOT/applicationContext.xml");
        Coach theCoach = context.getBean("myCoach", CricketCoach.class);
        System.out.println(theCoach.getDailyFortune());
        context.close();
    }
}
