package lab2c9;

public interface Coach {
    String getDailyWorkout();
    String getDailyFortune();
}
