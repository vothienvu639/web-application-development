package lab2c6;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("lab2c6/applicationContext.xml");
        BaseballCoach baseballCoach = (BaseballCoach) context.getBean("myCoach");
        System.out.println(baseballCoach.getDailyWorkout());
    }
}
