package lab2c6;


import lab2c5.Coach;
import lab2c5.FortuneService;

public class BaseballCoach implements Coach {
    @Override
    public String getDailyWorkout() {
        return fortuneService.getFortune();
    }

    @Override
    public String getDailyFortune() {
        return null;
    }

    private FortuneService fortuneService;

    public BaseballCoach(FortuneService theFortuneService){
        fortuneService = theFortuneService;
    }
}
