<!DOCTYPE html>
<html>
<body>
<form action="processForm" modelAttribute="student">
    First name: <input type="text" name="firstName" />
    <br/>
    Last name: <input type="text" name="lastName" />
    <br/>
    Country: <select name="country">
    <option items="${student.countryOptions}"/>
    <option value="Brazil" label="Brazil">
    <option value="France" label="France">
    <option value="Germany" label="Germany">
    <option value="India" label="India">
    </select>
    <br/>
    Favorite Language:

    Java <input type="radio" name="favoriteLanguage" value="Java"/>
    C# <input type="radio" name="favoriteLanguage" value="C#"/>
    PHP <input type="radio" name="favoriteLanguage" value="PHP"/>
    Ruby <input type="radio" name="favoriteLanguage" value="Ruby"/>
    <br/>
    <input type="submit" value="Submit" />
</form>
</body>
</html>