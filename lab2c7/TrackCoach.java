package lab2c7;

public class TrackCoach implements Coach {
    private FortuneService fortuneService;

    public TrackCoach(FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyWorkout(){
        return "Run a hark 5k";
    }

    @Override
    public String getDailyFortune() {
        return "Just Do It: " + this.fortuneService.getFortune();
    }
}
