package lab2c7;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("lab2c7/applicationContext.xml");
        Coach theCoach = context.getBean("myCoach", Coach.class);
        System.out.println(theCoach.getDailyWorkout());
        System.out.println(theCoach.getDailyFortune());
        Coach theCoach1 = context.getBean("trackCoach", Coach.class);
        System.out.println(theCoach1.getDailyWorkout());
        System.out.println(theCoach1.getDailyFortune());
        context.close();
    }
}
