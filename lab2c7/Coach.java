package lab2c7;

public interface Coach {
    String getDailyWorkout();
    String getDailyFortune();
}
