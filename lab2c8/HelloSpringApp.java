package lab2c8;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("lab2c8/applicationContext.xml");
        CricketCoach theCoach = context.getBean("myCoach", CricketCoach.class);
        FortuneService fortuneService = new HappyFortuneService();
        theCoach.setFortuneService(fortuneService);
        System.out.println(theCoach.getDailyFortune());
        context.close();
    }
}
