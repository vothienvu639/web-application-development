package lab2c8;

public interface Coach {
    String getDailyWorkout();
    String getDailyFortune();
}
