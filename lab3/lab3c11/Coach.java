package lab3.lab3c11;

public interface Coach {
    public String getDailyWorkout();
    public String getDailyFortune();
}
