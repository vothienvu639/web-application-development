package lab3.lab3c13;

public interface Coach {
    public String getDailyWorkout();
    public String getDailyFortune();
}
