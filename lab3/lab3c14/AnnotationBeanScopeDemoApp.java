package lab3.lab3c14;


import lab3.lab3c13.Coach;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AnnotationBeanScopeDemoApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);
        Coach theCoach = context.getBean("tennisCoach", Coach.class);
        System.out.println(theCoach.getDailyFortune());
        System.out.println(theCoach.getDailyWorkout());
        context.close();
    }
}
