package lab3.lab3c14;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("lab3c14")
public class SportConfig {
}
