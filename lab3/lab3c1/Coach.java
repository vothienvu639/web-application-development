package lab3.lab3c1;

public interface Coach {
    String getDailyWorkout();
    String getDailyFortune();
}
