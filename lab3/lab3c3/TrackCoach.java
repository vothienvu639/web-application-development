package lab3.lab3c3;

public class TrackCoach implements Coach {
    @Override
    public  void doMyStartupStuff(){
        System.out.println("TrackCoach: inside method doMyStartupStuff");
    }
    @Override
    public void doMyCleanupStuffYoYo(){
        System.out.println("TrackCoach: inside method doMyCleanupStuffYoYo");
    }
    private FortuneService fortuneService;

    public TrackCoach(FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyWorkout() {
        return "Run a hark 5k";
    }

    @Override
    public String getDailyFortune() {
        return this.fortuneService.getFortune();
    }
}
