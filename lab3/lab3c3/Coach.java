package lab3.lab3c3;

public interface Coach {
    void doMyStartupStuff();
    void doMyCleanupStuffYoYo();
    String getDailyWorkout();
    String getDailyFortune();
}
