package lab3.lab3c9;

public interface Coach {
    public String getDailyWorkout();
    public String getDailyFortune();
}
