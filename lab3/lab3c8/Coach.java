package lab3.lab3c8;

public interface Coach {
    public String getDailyWorkout();
    public String getDailyFortune();
}
