package lab3.lab3c2;

public interface Coach {


    String getDailyWorkout();
    String getDailyFortune();
}
