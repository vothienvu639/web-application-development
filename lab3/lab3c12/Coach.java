package lab3.lab3c12;

public interface Coach {
    public String getDailyWorkout();
    public String getDailyFortune();
}
