package lab3.lab3c15;

public interface Coach {
    public String getDailyWorkout();
    public String getDailyFortune();
}
