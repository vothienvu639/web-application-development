package lab3.lab3c10;

public interface Coach {
    public String getDailyWorkout();
    public String getDailyFortune();
}
