package lab2c5;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("lab2c5/applicationContext.xml");
        HappyFortuneService happyFortuneService = (HappyFortuneService) context.getBean("myCoach");
        System.out.println(happyFortuneService.getFortune());
    }
}
