package lab2c5;

public interface Coach {
    public String getDailyWorkout();
    public String getDailyFortune();
}
