<!DOCTYPE html>
<html>
<head>
<title>Student Confirmation</title>
</head>
<body>
The student is confirmed: ${student.firstName} ${student.lastName}
<br/>
Country: ${student.country}
<br/>
 Favorite Language: ${student.favoriteLanguage}

<ul>
<forEach var="temp" items="${student.operationSystems">
<li>${temp}</li>
</forEach>
</ul>
</body>
</html>