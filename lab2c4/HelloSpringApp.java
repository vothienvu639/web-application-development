package lab2c4;

import lab2c1.BaseballCoach;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("lab2c4/applicationContext.xml");
        BaseballCoach baseballCoach = (BaseballCoach) context.getBean("myCoach");
        System.out.println(baseballCoach.getDailyWorkout());
    }
}
