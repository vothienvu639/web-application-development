package lab2c3;

import lab2c2.TrackCoach;
import org.springframework.beans.BeansException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {
    public static void main(String[] args) throws BeansException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("lab2c3/applicationContext.xml");
        TrackCoach trackCoach = (TrackCoach) context.getBean("myCoach");
        System.out.println(trackCoach.getDailyWorkout());
    }
}
