<!DOCTYPE html>
<html>
<body>
<form action="processForm" modelAttribute="student">
    First name: <input type="text" name="firstName" />
    <br/>
    Last name: <input type="text" name="lastName" />
    <br/>
    Country: <select name="country">
    <option items="${student.countryOptions}"/>
    <option value="Brazil" label="Brazil">
    <option value="France" label="France">
    <option value="Germany" label="Germany">
    <option value="India" label="India">
    </select>
    <br/>
    <input type="submit" value="Submit" />
</form>
</body>
</html>